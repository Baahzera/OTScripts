local config = {
	newPosition = {x = 95, y = 117, z = 7},
	storage = 42042,
	removerItem = true
	}
	
function onUse(player, item, fromPosition, target, toPosition, isHotkey)
		if player:getStorageValue(config.storage) == 0 then
			player:getPosition():sendMagicEffect(CONST_ME_POFF)
			player:teleportTo(config.newPosition, true)
			player:getPosition():sendMagicEffect(CONST_ME_ENERGYAREA)
			player:setStorageValue(config.storage, 1)
		else
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "Voce nao pode teleportar no momento.")
		end
		if config.removerItem then
			item:remove(1)
		end
		return true
end
