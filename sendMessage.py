'''
Script escrito por Luiz Henrque Marsilio [Yzef]

1. rode o script e clique na janela do jogo
2. não mexa em nada até o script terminar

Dependendo da estrutura do site o script pode não funcionar corretamente.
Se esse for o caso, faça as alterações necessárias.
'''
import requests
import time
from lxml import html
from pyautogui import press, typewrite, hotkey
session = requests.Session() # bypass https
url = 'https://www.impera-global.com/?subtopic=worlds&world=Impera-Global' # url do whoisonline completa, com a tabela dos players online
msg = 'Written by Yzef.' # mensagem a ser enviada
page = session.get(url, cookies={'from-my': 'browser'}, verify=False) # faz download do html
tree = html.fromstring(page.content) # cria estrutura do html
result = tree.xpath('//td/a/text()') # cria lista de nomes
time.sleep(1) # aguarda 1s
for names in result[4:]: # alternar o 4 para maior ou menor caso o primeiro nome na lista fique incorreto
    typewrite('*' + names + '* ' + msg, interval=0.05) # escreve o nome do player e a mensagem a ser enviada
    press('enter') # pressiona enter para enviar a mensagem
    time.sleep(1) # aguarda 1s
    hotkey('ctrl', 'a') # seleciona todo texo
    press('backspace') # apaga texto selecionado
    time.sleep(5) # aguarda 5s (para nao dar muted)