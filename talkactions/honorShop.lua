-- lista dos items a serem comercializados pelo sistema de honra
item = {
	["boots of haste"] = {id = 2195, price = 100}
	}
	
function onSay(player, words, param)
		
	choise = item[param] -- seta o indice da tabela que sera acessado id e price
	if choise then -- se foi digitado alguma coisa de parametro continuar
		if getHonor(player) >= choise.price then -- verifica se a honra do player é suficiente para esse item escolhido (choise)
			player:addItem(choise.id, 1) -- add o item escolhido (id)
			remHonor(player, choise.price) -- desconta honor do player (price)
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "You have received " .. ItemType(choise.id):getName()) -- mensagem confirmando o recebimento. busca o nome do item por id
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "Your current Honor balance is " .. getHonor(player)) -- informa o saldo de honra atual
		else
			player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "Your Honor is not enough to buy this item.") -- mensagem de erro caso nao tenha honra suficiente
		end
	else
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "Available items on Honor Store: ") -- se nao houver parametro
		for i, j in pairs(item) do -- busca na lista os dados dos items disponiveis
		player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "Item: [" .. i .. "]. Price: [" .. j.price .. "].") -- informa os dados encontrados através de mensagem
		end
	end
	return false
end
