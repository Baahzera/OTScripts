config = {
	id = 2195, -- id do item
	bless = 6, -- quantidade de blessings do servidor
	remover = true, -- remover item? true/false
	minLvl = 100 -- level minimo
	}
	
-- hasAllBlessings(player) by Luiz Henrique Marsilio. 
function hasAllBlessings(player) --Verifica se o player tem todas blessings
	local cont = 0
	for i=1,config.bless do
		if player:hasBlessing(i) then
			cont = cont+1
		end
	end
	if cont >= config.bless then -- se nao tiver alguma bless prossegue com o sitema
		return true
	else
		return false
	end
end

function onPrepareDeath(creature, killer)

	if not creature:isPlayer() then
		return true
	end
	if hasAllBlessings(creature) then -- function by Luiz Henrique Marsilio
		creature:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "You are already protected by blessings.")
		return true
	end
	if creature:getLevel() >= config.minLvl then
		if creature:getItemCount(config.id) >= 1 then
			for i = 1, config.bless do
				creature:addBlessing(i)
			end
			if config.remover then
				creature:removeItem(config.id, 1)
				creature:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "You were protected by a blessing charm.")
			end
			creature:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "You have been blessed by the charm before dying.")
		else
			creature:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "You were not wearing a blessing charm and was not blessed when died.")
		end
	else
		creature:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "You need level " .. config.minLvl .. " or higher to be automatic blessed.")
	end
	return true
end