function onKill(player, target)
   if not target:isPlayer() then
       return true
   end
   -- exemplo de honra sendo adicionada ao matar outro player
   local honor = math.floor(target:getLevel()/10)
   addHonor(player, honor)
   player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "Your honor points have been increased in " .. honor .. ".")
   player:sendTextMessage(MESSAGE_STATUS_CONSOLE_ORANGE, "You have now " .. getHonor(player) .. " honor points.")
   -- pode ser estabelecido para condições serem satisfeitas para que a honra seja adicionada nesse caso
   -- por exemplo config = {level = 100} if target:getLevel() >= config.level then /stuff end
end