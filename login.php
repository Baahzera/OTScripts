<?php
/**
 * Created by Notepad++.
 * User: Malucooo - Erick Nunes
 * Remaked of login.php by JLCVP and parts of login.php by Monteiro. Thanks for both!
 * Date: 18/09/17
 * Time: 03:01
 */

require 'config/config.php';

// comment to show E_NOTICE [undefinied variable etc.], comment if you want make script and see all errors
error_reporting(E_ALL ^ E_STRICT ^ E_NOTICE);

// true = show sent queries and SQL queries status/status code/error message
define('DEBUG_DATABASE', false);
define('INITIALIZED', true);

if (!defined('ONLY_PAGE'))
    define('ONLY_PAGE', true);

// check if site is disabled/requires installation
include_once('./system/load.loadCheck.php');

// fix user data, load config, enable class auto loader
include_once('./system/load.init.php');

// DATABASE
include_once('./system/load.database.php');
if (DEBUG_DATABASE)
    Website::getDBHandle()->setPrintQueries(true);

// DATABASE END
/*error example:
{
    "errorCode":3,
    "errorMessage":"Account name or password is not correct."
}*/

# Declare variables with array structure
$characters = array();
$playerData = array();
$data = array();
$isCasting = false;

# error function
function sendError($msg){
    $ret = array();
    $ret["errorCode"] = 3;
    $ret["errorMessage"] = $msg;
    
    die(json_encode($ret));
}

# getting infos
	$request = file_get_contents('php://input');
	$result = json_decode($request, true);

# account infos
	$accountName = $result["accountname"];
	$password = $result["password"];

# game port
	$port = 7172;

// aqui começa a gambiarra
$orderby = 'name';
$players_online_data = $SQL->query('SELECT ' . $SQL->tableName('accounts') . '.' . $SQL->fieldName('flag') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('name') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('vocation') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('level') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('skull') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('looktype') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('lookaddons') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('lookhead') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('lookbody') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('looklegs') . ', ' . $SQL->tableName('players') . '.' . $SQL->fieldName('lookfeet') . ' FROM ' . $SQL->tableName('accounts') . ', ' . $SQL->tableName('players') . ', ' . $SQL->tableName('players_online') . ' WHERE ' . $SQL->tableName('players') . '.' . $SQL->fieldName('id') . ' = ' . $SQL->tableName('players_online') . '.' . $SQL->fieldName('player_id') . ' AND ' . $SQL->tableName('accounts') . '.' . $SQL->fieldName('id') . ' = ' . $SQL->tableName('players') . '.' . $SQL->fieldName('account_id') . ' ORDER BY ' . $SQL->fieldName($orderby))->fetchAll();
$number_of_players_online = 0;
foreach($players_online_data as $player)
{
	$number_of_players_online++; 
}	
// aqui terminamos a gambiarra
	
# cache infos by: Luiz Marsilio
	$cacheInfo = array();
	$cacheInfo = $result["type"];
	
	if ($cacheInfo == "cacheinfo") {
	$cache = array(
		"gamingyoutubestreams" => 0,
		"gamingyoutubeviewer" => 0,
		"playersonline" => $number_of_players_online,
		"twitchstreams" => 0,
		"twitchviewer" => 0,
	);
		echo json_encode($cache);
	} else { # else, caso contrário retorna todo JSON de cacheinfo e o JSON de login juntos. Queremos apenas o JSON de cache ou o JSON de login
			# check if player wanna see cast list
			if (strtolower($accountName) == "cast")
				$isCasting = true;
			if ($isCasting) {
				$casts = $SQL->query("SELECT `player_id` FROM `live_casts`")->fetchAll();
				if (count($casts[0]) == 0)
					sendError("There is no live casts right now!");
				foreach($casts as $cast) {
					$character = new Player();
					$character->load($cast['player_id']);
					
					if ($character->isLoaded()) {
						$char = array("ismale" => (($character->getSex() == 1) ? True : False), "name" => $character->getName(), "tutorial" => false, "worldid" => 0);
						$characters[] = $char;
					}			
				}
				$port = 7173;
				$lastLogin = 0;
				$premiumAccount = true;
				$timePremium = 0;
			} else {
				$account = new Account();
				$account->find($accountName);
				
				if (!$account->isLoaded())
					sendError("Failed to get account. Try again!");
				if ($account->getPassword() != Website::encryptPassword($password))
					sendError("The password for this account is wrong. Try again!");
				
				foreach($account->getPlayersList() as $character) {
					$char = array("ismale" => (($character->getSex() == 1) ? True : False), "name" => $character->getName(), "tutorial" => false, "worldid" => 0);
					$characters[] = $char;
				}
				
				$lastLogin = $account->getLastLogin();
				$premiumAccount = ($account->isPremium()) ? true : false;
				$timePremium = time() + ($account->getPremDays() * 86400);
			}
			$session = array(
				"fpstracking" => False,
				"ispremium" => $premiumAccount,
				"isreturner" => False,
				"lastlogintime" => $lastLogin,
				"optiontracking" => False,
				"premiumuntil" => $timePremium,
				"returnernotification" => False,
				"sessionkey" => $accountName . "\n" . $password,
				"showrewardnews" => False, 
				"status" => "active",
				);
				
			$world = array(
				"anticheatprotection" => False,
				"externaladdressprotected" => $config['server']['ip'],
				"externaladdressunprotected" => $config['server']['ip'],
				"externalportprotected" => $port,
				"externalportunprotected" => $port,
				"id" => 0,
				"location" => "BRA",
				"name" => $config['server']['serverName'],
				"previewstate" => 0,
				);
				

			//Survey by: Cjaker
			$survey = array(
				"id" => rand(0, 999999),
				"invitationtext" => "Cjaker survey test",
				"invitationtoken" => "1751f1beddf001e1d36dee78ace974",
				"endtimestamp" => 1510614000
			);

			// https://limesurvey.cipsoft.com/index.php/survey/index/sid/527875/lang-en?token=1751f1beddf001e1d36dee78ace974
			// token=invitationtoken
			// o endtimestamp acima é o tempo convertido em unix timestamp, onde o mesmo é o prazo que irá acabar o survey!

			$worlds = array($world);
			$data["session"] = $session;
			$playerData["worlds"] = $worlds;
			$playerData["characters"] = $characters;
			$data["playdata"] = $playerData;
			$data["survey"] = $survey;
			echo json_encode($data);
}

